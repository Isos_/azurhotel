# ***Azur Hotel*** #

---
Le projet **Azur** concerne le développement d’une application mobile (Android) téléchargeable ou accessible sur le site du groupe Azur.

L’application mobile permettra aux clients d’accéder aux hôtels et à leurs réservations via un web-service d’accès aux données SqlServer ou MySql.

### Fonctionnalités de l’application

L’application mobile devra offrir aux clients les fonctionnalités suivantes :

* Menu principal d’accès aux pages de l’application
* Afficher tous les hôtels : nom, adresse, tel, description, prix, …
* Annuler une réservation par le client en saisissant son n°réservation et son codeaccess.

---
### Ressources

Versions : [Local](https://bitbucket.org/Isos_/azurhotel/get/master.zip) (Sqlite) **|** [Web](https://bitbucket.org/Isos_/azurhotel/get/masterWeb.zip) (JSon - MySQL)

Accès a la Base de donnée ([phpMyAdmin](http://sio2js.tk/mydb)) :

* Identifiant : sio2
* Mot de passe : sio2pass
* nom_bdd : azurHotel

---

### Base de Donnée associé a l'application

L'application possede actuellement 2 versions en fonction des base de donnée :

* SqlLite (BDD local)
* Mysql (Web Service)

Schéma (**UML**) de la Base de donnée :

![](https://bitbucket.org/Isos_/azurhotel/downloads/2016-04-12_21-30-10.png)

---
### Web Service

Le Web Service est utilisé par l'application via les url que celle-ci exécute, par exemple lors de la récupération d'une liste d'hotels :

`http://sio2js.tk/libExt/webserviceazurhotel/api.php?action=get&var=get_hotels`

Que l'application pourra traiter en recevant des données sous un format JSon :

```

#!json
{
  "hotels": [
    {
      "id": "1",
      "nom": "Posuere Vulputate Lt",
      "adresse": "Ap #866-3548 In Road",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque",
      "tel": "07 85 43 97 87",
      "rating": "5"
    }
  ]
}
```

Et instancier les hotels reçus dans cette liste :

```

#!java

try {
     JSONObject jsonObject = new JSONObject(dataJ);
     hotels = new ArrayList<>();
     JSONArray jsonArray = jsonObject.getJSONArray("hotels");

     for (int i = 0; i < jsonArray.length(); i++) {
         JSONObject a = jsonArray.getJSONObject(i);


         String id = a.getString("id");
         String nom = a.getString("nom");
         String adresse = a.getString("adresse");
         String desc = a.getString("description");
         String tel = a.getString("tel");
         String rating = a.getString("rating");

         hotels.add(new hotel(Long.valueOf(id), nom, adresse, desc, tel, Float.valueOf(rating)));

     }


} catch (JSONException e) {
     e.printStackTrace();
}
```