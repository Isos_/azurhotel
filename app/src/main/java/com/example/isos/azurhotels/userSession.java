package com.example.isos.azurhotels;

import android.app.Application;
import java.util.ArrayList;

public class userSession extends Application {

    private long id;
    private String name;
    private String lastname;
    private String email;
    private String password;


    @Override
    public void onCreate() {
        super.onCreate();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
