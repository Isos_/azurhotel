package com.example.isos.azurhotels;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class res_hotelActivity extends AppCompatActivity {

    public static Activity desActivity;
    private ArrayList<hotel> listHotels = new ArrayList<>();
    private ArrayList<hotel> listHotelsRes;
    private DataBaseAzur dataBaseAzur;
    private userSession userSession;
    private long idHotel;
    private boolean msg = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res_hotel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        desActivity = this;
        Bundle data = getIntent().getExtras();
        String hotelName = data.getString("hotelName");
        String hotelDesc = data.getString("hotelDesc");
        float hotelRating = data.getFloat("hotelRating");
        String hotelPlace = data.getString("hotelPlace");
        String hotelTel = data.getString("hotelTel");
        idHotel = data.getLong("hotelId");
        toolbar.setTitle(hotelName);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RatingBar ratingBar = (RatingBar) findViewById(R.id.res_rating);
        ratingBar.setRating(hotelRating);

        TextView desc = (TextView) findViewById(R.id.resDesc);
        desc.setText(hotelDesc);

        TextView place = (TextView) findViewById(R.id.resPlace);
        place.setText("Adresse : " + hotelPlace);

        TextView tel = (TextView) findViewById(R.id.resTel);
        tel.setText("Téléphone : " + hotelTel);
    }


    public void onClick(View view){

        dataBaseAzur = new DataBaseAzur(view.getContext());
        userSession = (userSession) view.getContext().getApplicationContext();

        Cursor cursor = dataBaseAzur.getTable(hotel.TABLE_NAME);

        if(cursor != null){
            while(cursor.moveToNext()){
                listHotels.add(new hotel(Long.valueOf(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Float.valueOf(cursor.getString(5))));
            }
        }

        listHotelsRes = dataBaseAzur.getHotelRes(userSession.getId(), listHotels);

        for(hotel hotel : listHotelsRes){
            if(hotel.getId() == idHotel){
                msg = false;
            }
        }

        if(msg){
            boolean isInserted = dataBaseAzur.insertHotelRes(userSession.getId(), idHotel);

            if(isInserted)
                Toast.makeText(view.getContext(), "Réservation effectué", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(view.getContext(), "Impossible d'éffectuer la réservation", Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(view.getContext(), "Cet Hotel est déjà réservé", Toast.LENGTH_LONG).show();
        }


    }

}
