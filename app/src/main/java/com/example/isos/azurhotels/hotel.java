package com.example.isos.azurhotels;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class hotel {

    private long id;
    private String name;
    private String place;
    private String description;
    private String tel;
    private float rating;
    private static ArrayList<hotel> listHotel = new ArrayList<>();

    //Database table
    public static final String TABLE_NAME = "hotel";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PLACE = "place";
    public static final String COLUMN_DESC = "description";
    public static final String COLUMN_TEL = "telephone";
    public static final String COLUMN_RATING = "rating";

    //Creation Table
    private static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT NOT NULL, "
            + COLUMN_PLACE + " TEXT NOT NULL, "
            + COLUMN_DESC + " TEXT NOT NULL, "
            + COLUMN_TEL + " INTEGER NOT NULL, "
            + COLUMN_RATING + " REAL NOT NULL "
            +");";


    public static void onCreate(SQLiteDatabase db){
        db.execSQL(TABLE_CREATE);
    }

    public hotel(){}

    public hotel(long id, String name, String place, String description, String tel, float rating){

        this.id = id;
        this.name = name;
        this.place = place;
        this.tel = tel;
        this.rating = rating;
        this.description = description;

    }

    public static ArrayList<hotel> getListHotel() {
        return listHotel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String  tel) {
        this.tel = tel;
    }
}
