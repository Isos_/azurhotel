package com.example.isos.azurhotels;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DataBaseAzur extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "azurdb";

    //TABLE HOTEL_USER_RES
    public static final String TABLE_NAME_HOTELRES = "hotel_user_res";
    public static final String COLUMN_HOTELRES_IDUSER = "iduser";
    public static final String COLUMN_HOTELRES_IDHOTEL = "idhotel";
    public static final String COLUMN_HOTELRES_NUMRES = "numres";


    public DataBaseAzur(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        hotel.onCreate(db);
        user.onCreate(db);
        onCreateHotelRes(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + hotel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + user.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_HOTELRES);
        onCreate(db);
        Log.i("DB", "Upgrade");
    }

    public void onCreateHotelRes(SQLiteDatabase db){
        db.execSQL("CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME_HOTELRES + "("
                + COLUMN_HOTELRES_IDUSER + " INTEGER, "
                + COLUMN_HOTELRES_IDHOTEL + " INTEGER, "
                + COLUMN_HOTELRES_NUMRES + " INTEGER, "
                + "FOREIGN KEY(" + COLUMN_HOTELRES_IDUSER + ") REFERENCES " + user.TABLE_NAME + "(" + user.COLUMN_ID + ")"
                + ");");
    }


    //INSERT Methods
    public boolean insertHotel(String name, String place, String description, String tel, float rating){

        SQLiteDatabase db = this.getWritableDatabase();
        String ratingS = String.valueOf(rating);

        ContentValues values = new ContentValues();
        values.put(hotel.COLUMN_NAME, name);
        values.put(hotel.COLUMN_PLACE, place);
        values.put(hotel.COLUMN_DESC, description);
        values.put(hotel.COLUMN_TEL, tel);
        values.put(hotel.COLUMN_RATING, ratingS);
        long res = db.insert(hotel.TABLE_NAME, null, values);

        if(res == -1)
            return false;
        else
            return true;

    }

    public boolean insertUser(String name, String lastname, String email, String password){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(user.COLUMN_NAME, name);
        values.put(user.COLUMN_LASTNAME, lastname);
        values.put(user.COLUMN_EMAIL, email);
        values.put(user.COLUMN_PASSWORD, password);
        long res = db.insert(user.TABLE_NAME, null, values);

        if(res == -1)
            return false;
        else
            return true;

    }

    public boolean insertHotelRes(long idUser, long idHotel){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_HOTELRES_IDUSER, String.valueOf(idUser));
        values.put(COLUMN_HOTELRES_IDHOTEL, String.valueOf(idHotel));
        values.put(COLUMN_HOTELRES_NUMRES, String.valueOf(idUser) + String.valueOf(idHotel));
        long res = db.insert(TABLE_NAME_HOTELRES, null, values);

        if(res == -1)
            return false;
        else
            return true;
    }
    //INSERT Methods <End>

    //GET Methods
    public Cursor getTable(String tableName){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("Select * FROM " + tableName, null);
        return res;
    }

    public ArrayList<hotel> getHotelRes(long idUser, ArrayList<hotel> hotelArrayList){
        Cursor cursor = this.getTable(TABLE_NAME_HOTELRES);
        ArrayList<hotel> hotels = new ArrayList<>();

        while(cursor.moveToNext()){
            if(Long.valueOf(cursor.getString(0)) == idUser){
                for(hotel hotel : hotelArrayList){
                    if(Long.valueOf(cursor.getString(1)) == hotel.getId()){
                        hotels.add(hotel);
                    }
                }
            }
        }

        return hotels;
    }
    //GET Methods <End>

    //DELETE Methods
    public int deleteHotelRes(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME_HOTELRES, COLUMN_HOTELRES_IDHOTEL + " = ?", new String[] {String.valueOf(id)});
    }
    //DELETE Methods <End>

    //UDATE Methods
    public boolean updateUser(long id, String name, String lastName, String email){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(user.COLUMN_ID, String.valueOf(id));
        values.put(user.COLUMN_NAME, name);
        values.put(user.COLUMN_LASTNAME, lastName);
        values.put(user.COLUMN_EMAIL, email);
        db.update(user.TABLE_NAME, values, "ID = ? ", new String[]{String.valueOf(id)});

        return true;

    }
}
