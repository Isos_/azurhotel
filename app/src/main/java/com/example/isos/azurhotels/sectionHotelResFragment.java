package com.example.isos.azurhotels;


import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class sectionHotelResFragment extends Fragment {

    RecyclerView listHotelRes;
    adapterHotelRes adapterHotelRes;
    LinearLayoutManager linear;
    ArrayList<hotel> listHotels = new ArrayList<>();
    DataBaseAzur dataBaseAzur;
    userSession userSession;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.section_hotel_res_fragment, container, false);
        dataBaseAzur = new DataBaseAzur(view.getContext());
        userSession = (userSession) view.getContext().getApplicationContext();

        Cursor cursor = dataBaseAzur.getTable(hotel.TABLE_NAME);

        if(cursor != null){
            while(cursor.moveToNext()){
                listHotels.add(new hotel(Long.valueOf(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Float.valueOf(cursor.getString(5))));
            }
        }


        linear = new LinearLayoutManager(view.getContext());
        adapterHotelRes = new adapterHotelRes(view.getContext());
        adapterHotelRes.setListHotels(dataBaseAzur.getHotelRes(userSession.getId(), listHotels));
        listHotelRes = (RecyclerView) view.findViewById(R.id.recyclerRes);
        listHotelRes.setLayoutManager(linear);
        listHotelRes.setAdapter(adapterHotelRes);

        return view;
    }
}
