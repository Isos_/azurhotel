package com.example.isos.azurhotels;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.logging.LogRecord;

public class adapterHotelRes extends RecyclerView.Adapter {

    public static boolean delItem = false;
    public static boolean delay = false;
    private static long idCurrentHotel;
    private static int adapterPosition;
    private ArrayList<hotel> listHotels = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private View view;
    private String numRes;
    private DataBaseAzur dataBaseAzur;
    private userSession userSession;
    private cancelResDialog resDialog = new cancelResDialog();
    private FragmentManager manager;

    public adapterHotelRes(Context context){
        layoutInflater = layoutInflater.from(context);
        dataBaseAzur = new DataBaseAzur(context);
        userSession = (userSession) context.getApplicationContext();
        manager = ((Activity) context).getFragmentManager();
    }

    public void setListHotels(ArrayList<hotel> listHotels) {
        this.listHotels = listHotels;
    }

    public void delete(int position){
        listHotels.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = layoutInflater.inflate(R.layout.item_list_cardview_user, parent, false);
        viewHolderHotel viewHolder = new viewHolderHotel(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final hotel currentHotel = listHotels.get(position);
        viewHolderHotel viewHolderHotel = (viewHolderHotel)holder;


        Cursor cursor = dataBaseAzur.getTable(dataBaseAzur.TABLE_NAME_HOTELRES);
        while(cursor.moveToNext()){
                if(Long.valueOf(cursor.getString(1)) == currentHotel.getId()){
                    numRes = cursor.getString(2);
                }
        }

        final String numResCancel = numRes;

        viewHolderHotel.hotelName.setText(currentHotel.getName());
        viewHolderHotel.hotelPlace.setText(currentHotel.getPlace());
        viewHolderHotel.numRes.setText(numRes);
        viewHolderHotel.hotelRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                resDialog.setData(numResCancel, userSession.getPassword());
                resDialog.show(manager, "popup_res");
                adapterPosition = holder.getAdapterPosition();
                idCurrentHotel = currentHotel.getId();


            }

        });

        viewHolderHotel.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delItem && idCurrentHotel == currentHotel.getId()){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dataBaseAzur.deleteHotelRes(idCurrentHotel);
                            delete(adapterPosition);
                        }
                    }, 500);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return listHotels.size();
    }


    static class viewHolderHotel extends RecyclerView.ViewHolder {

        private TextView hotelName;
        private TextView hotelPlace;
        private ImageButton hotelRemove;
        private TextView numRes;

        public viewHolderHotel(final View view){
            super(view);

            hotelName = (TextView) view.findViewById(R.id.hotelName);
            hotelPlace = (TextView) view.findViewById(R.id.hotelPlace);
            hotelRemove = (ImageButton) view.findViewById(R.id.buttonDelRes);
            numRes = (TextView) view.findViewById(R.id.numRes);

            if(delay){
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        view.performClick();
                    }
                });
            }


        }

    }
}
