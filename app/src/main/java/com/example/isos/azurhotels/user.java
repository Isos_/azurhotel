package com.example.isos.azurhotels;


import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class user {

    private long id;
    private String name;
    private String lastname;
    private String email;
    private String password;
    private ArrayList<hotel> hotelRes;

    //Database table
    public static final String TABLE_NAME = "user";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LASTNAME = "lastname";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASSWORD = "password";

    //Creation Table
    private static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT NOT NULL, "
            + COLUMN_LASTNAME + " TEXT NOT NULL, "
            + COLUMN_EMAIL + " TEXT NOT NULL, "
            + COLUMN_PASSWORD + " TEXT NOT NULL "
            +");";


    public static void onCreate(SQLiteDatabase db){
        db.execSQL(TABLE_CREATE);
    }

    public user(long id ,String name, String lastname, String email, String password){

        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        hotelRes = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<hotel> getHotelRes() {
        return hotelRes;
    }

    public void setHotelRes(ArrayList<hotel> hotelRes) {
        this.hotelRes = hotelRes;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
