package com.example.isos.azurhotels;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

public class list_hotelActivity extends AppCompatActivity {

    ArrayList<hotel> lstHotel = new ArrayList<>();
    RecyclerView listHotel;
    adapterHotel adapterHotel;
    LinearLayoutManager linear;
    DataBaseAzur dataBaseAzur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_hotel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dataBaseAzur = new DataBaseAzur(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), userActivity.class);
                startActivity(intent);
            }
        });

        //Instances des hotels
        Cursor cursor = dataBaseAzur.getTable(hotel.TABLE_NAME);

        if(cursor != null){
            while(cursor.moveToNext()){
                lstHotel.add(new hotel(Long.valueOf(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), Float.valueOf(cursor.getString(5))));
            }
        }

        linear = new LinearLayoutManager(this);

        listHotel = (RecyclerView) findViewById(R.id.lstHotels);
        adapterHotel = new adapterHotel(this);
        adapterHotel.setListHotels(lstHotel);
        listHotel.setLayoutManager(linear);
        listHotel.setAdapter(adapterHotel);
    }
}
