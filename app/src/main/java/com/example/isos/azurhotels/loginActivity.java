package com.example.isos.azurhotels;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;

public class loginActivity extends AppCompatActivity {

    ArrayList<user> lstUsers = new ArrayList<>();
    EditText userNameText;
    EditText userPasswordText;
    user userLog;
    userSession userSession;
    DataBaseAzur dataBaseAzur;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dataBaseAzur = new DataBaseAzur(this);


//        Insert Hotels Database
//        dataBaseAzur.insertHotel("Enim Associates",
//                "Ap #116-8444 Rutrum Ave",
//                "non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie",
//                "01 41 92 73 03",
//                2);
//        dataBaseAzur.insertHotel("Gravida Nunc",
//                "Ap #273-2378 Arcu. Rd.",
//                "mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
//                "07 37 96 81 87",
//                5);
//
//        dataBaseAzur.insertHotel("Viverra Donec Tempus Consulting",
//                "275 Consectetuer, Street",
//                "bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue",
//                "08 17 69 22 46",
//                1);
//        dataBaseAzur.insertHotel("Aliquet Vel LLC",
//                "Ap #101-6558 Per Road",
//                "tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper",
//                "06 82 96 01 65",
//                3);
//        dataBaseAzur.insertHotel("Dignissim Maecenas Inc.",
//                "3354 Nam Street",
//                "accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque",
//                "09 22 51 34 43",
//                4);
//        dataBaseAzur.insertUser("User", "lastNameUser", "user.sio@email.com", "admin");

    }

    public void onClick(View view){
//        Toast.makeText(view.getContext(), "Lol, t'a cru quoi ?", Toast.LENGTH_LONG).show();

        Cursor cursor = dataBaseAzur.getTable(user.TABLE_NAME);

        while (cursor.moveToNext()){
            lstUsers.add(new user(Long.valueOf(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
        }


        userNameText = (EditText) findViewById(R.id.userNameText);
        userPasswordText = (EditText) findViewById(R.id.userPasswordText);

        String userName = userNameText.getText().toString();
        String userPassword = userPasswordText.getText().toString();
        Boolean userNameB = false;
        Boolean userPasswordB = false;
        int i = 0;

        for (user user : lstUsers){
            if(i<1){
                if(user.getName().equals(userName)  && user.getPassword().equals(userPassword)){

                    userNameB = true;
                    userPasswordB = true;
                    i++;
                    userLog = user;

                }
            }
        }


        if(!(userNameB)){
            if(!(userPasswordB)){
                Toast.makeText(view.getContext(), "Identifiants incorrectes", Toast.LENGTH_LONG).show();
            }
        }else {

            userSession = (userSession)getApplicationContext();
            userSession.setId(userLog.getId());
            userSession.setName(userLog.getName());
            userSession.setLastname(userLog.getLastname());
            userSession.setEmail(userLog.getEmail());
            userSession.setPassword(userLog.getPassword());
            Intent intent = new Intent(this, list_hotelActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public void onSub(View view){

        Intent i = new Intent(this, subscribeActivity.class);
        startActivity(i);
    }

}
