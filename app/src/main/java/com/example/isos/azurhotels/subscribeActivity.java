package com.example.isos.azurhotels;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class subscribeActivity extends AppCompatActivity {

    EditText nameText;
    EditText lastNameText;
    EditText EmailText;
    EditText passwordText;
    DataBaseAzur dataBaseAzur;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Inscription");
        setSupportActionBar(toolbar);
        dataBaseAzur = new DataBaseAzur(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void onClick(View view){

        nameText = (EditText) findViewById(R.id.nameSub);
        lastNameText = (EditText) findViewById(R.id.LastNameSub);
        EmailText = (EditText) findViewById(R.id.emailSub);
        passwordText = (EditText) findViewById(R.id.passSub);

        String name = nameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        String Email = EmailText.getText().toString();
        String password = passwordText.getText().toString();

        boolean isInserted = dataBaseAzur.insertUser(name, lastName, Email, password);

        if(isInserted){
            Intent i = new Intent(this, loginActivity.class);
            startActivity(i);
            finish();
            Toast.makeText(this, "Compte Crée", Toast.LENGTH_LONG).show();
        }else
            Toast.makeText(this, "Erreur : les informations ne sont pas renseigner", Toast.LENGTH_LONG).show();

    }


}
