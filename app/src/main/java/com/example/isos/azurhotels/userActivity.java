package com.example.isos.azurhotels;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class userActivity extends AppCompatActivity {

    public static Activity myaccount;
    userSession userSession;
    EditText userName;
    EditText userLastName;
    EditText userEmail;
    DataBaseAzur dataBaseAzur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myaccount = this;
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dataBaseAzur = new DataBaseAzur(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userSession = (userSession)getApplicationContext();
        userName = (EditText) findViewById(R.id.nameUserSession);
        userLastName = (EditText) findViewById(R.id.LastNameUserSession);
        userEmail = (EditText) findViewById(R.id.emailUserSession);

        userName.setText(userSession.getName());
        userLastName.setText(userSession.getLastname());
        userEmail.setText(userSession.getEmail());

    }


    public void onClick(View view){
//       boolean isUpdate = dataBaseAzur.updateUser(userSession.getId(), userSession.getName(), userSession.getLastname(), userSession.getEmail());
//
//        if(isUpdate){
//            Toast.makeText(view.getContext(), "Mise à jour effectué", Toast.LENGTH_LONG).show();
//        }else {
//            Toast.makeText(view.getContext(), "Impossible de mettre a jour", Toast.LENGTH_LONG).show();
//        }
    }


}
