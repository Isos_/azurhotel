package com.example.isos.azurhotels;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class cancelResDialog extends DialogFragment {

    private LayoutInflater inflater;
    private View view;
    private String numRes;
    private String password;
    private EditText numResinput;
    private EditText passCancelRes;
    private boolean delete = false;

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.popup_res, null);
        numResinput = (EditText) view.findViewById(R.id.numResInput);
        passCancelRes = (EditText) view.findViewById(R.id.passCancelRes);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Annuler la Réservation");
        builder.setIcon(R.drawable.ic_warning_black_24dp);
        builder.setView(view);
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(numResinput.getText().toString().equals(numRes) && passCancelRes.getText().toString().equals(password)){
                            dismiss();
                            Toast.makeText(view.getContext(), "Ok", Toast.LENGTH_SHORT).show();
                            adapterHotelRes.delItem = true;
                            adapterHotelRes.delay = true;
                            Intent refresh = new Intent(v.getContext(), userActivity.class);
                            userActivity.myaccount.finish();
                            userActivity.myaccount.overridePendingTransition(0, 0);
                            userActivity.myaccount.startActivity(refresh);
                            userActivity.myaccount.overridePendingTransition(0, 0);
                        }else {
                            Toast.makeText(view.getContext(), "Numero ou Mot de passe incorrecte", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });



        return alertDialog;
    }

    public void setData(String numRes, String password){
        this.numRes = numRes;
        this.password = password;
    }
}
