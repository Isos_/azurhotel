package com.example.isos.azurhotels;


import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class adapterHotel extends RecyclerView.Adapter {

    private ArrayList<hotel> listHotels = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private long idHotel;
    private String hotelNameStr;
    DataBaseAzur dataBaseAzur;

    public adapterHotel(Context context){
        layoutInflater = layoutInflater.from(context);
    }

    public void setListHotels(ArrayList<hotel> listHotels) {
        this.listHotels = listHotels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_list_cardview, parent, false);
        viewHolderHotel viewHolder = new viewHolderHotel(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final hotel currentHotel = listHotels.get(position);
        viewHolderHotel viewHolderHotel = (viewHolderHotel)holder;

        viewHolderHotel.imageHotel.setImageResource(R.drawable.hotel);
        viewHolderHotel.hotelName.setText(currentHotel.getName());
        viewHolderHotel.hotelPlace.setText(currentHotel.getPlace());
        viewHolderHotel.hotelStars.setRating(currentHotel.getRating());
        viewHolderHotel.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                adapterHotelRes.delay = false;
                ArrayList<hotel> listHotelRes;
                boolean msg = true;
                dataBaseAzur = new DataBaseAzur(v.getContext());
                userSession userSession = (userSession) v.getContext().getApplicationContext();
                listHotelRes = dataBaseAzur.getHotelRes(userSession.getId(), listHotels);

                for(hotel hotel : listHotelRes){
                    if(hotel.getId() == getItemId(position)){
                        msg = false;
                    }
                }

                if(msg){
                    boolean isInserted = dataBaseAzur.insertHotelRes(userSession.getId(), getItemId(position));

                    if(isInserted)
                        Snackbar.make(v, "Réservation effectué", Snackbar.LENGTH_LONG).show();
                    else
                        Snackbar.make(v, "Impossible d'éffectuer la réservation", Snackbar.LENGTH_LONG).show();

                }else{
                    Snackbar.make(v, "Cet Hotel est déjà réservé", Snackbar.LENGTH_LONG).show();
                }

            }
        });

        viewHolderHotel.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), res_hotelActivity.class);
                i.putExtra("hotelName", currentHotel.getName());
                i.putExtra("hotelRating", currentHotel.getRating());
                i.putExtra("hotelDesc", currentHotel.getDescription());
                i.putExtra("hotelPlace", currentHotel.getPlace());
                i.putExtra("hotelTel", currentHotel.getTel());
                i.putExtra("hotelId", currentHotel.getId());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        hotel currentHotel = listHotels.get(position);
        idHotel = currentHotel.getId();
        return idHotel;
    }

    @Override
    public int getItemCount() {
        return listHotels.size();
    }

    static class viewHolderHotel extends RecyclerView.ViewHolder{

        private ImageView imageHotel;
        private TextView hotelName;
        private TextView hotelPlace;
        private RatingBar hotelStars;
        private ImageButton buttonAdd;

        public viewHolderHotel(final View view){
            super(view);

            final ArrayList<hotel> listHotelsRes = new ArrayList<>();
            final DataBaseAzur dataBaseAzur = new DataBaseAzur(view.getContext());

            imageHotel = (ImageView) view.findViewById(R.id.imageHotel);
            hotelName = (TextView) view.findViewById(R.id.hotelName);
            hotelPlace = (TextView) view.findViewById(R.id.hotelPlace);
            hotelStars = (RatingBar) view.findViewById(R.id.hotelStars);
            buttonAdd = (ImageButton) view.findViewById(R.id.buttonAddRes);


        }
    }
}
